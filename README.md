This calculates a homography given known point correspondences with no errors.

This uses the math.js library to do matrix calculations.

This might be useful if you want to map some set of 4 coordinates to screen coordinates for example.

You need exactly 4 point correspondances for this to work. A1 -> B1, A2 -> B2, A3 -> B3, A4 -> B4. This means A1 in your original coordinate system should map to B1 in the target one, etc. The output will be a matrix that when multipled by A1 will produce B1.

Eg, B1 = H*A1

Points homogeneous and are composed of X,Y pairs with a 1 on the end. You'll need to 'rehomogenise' the result of multiplying a point be the homography for the numbers to make sense.

A1 = (ax1, ay1, 1)

## Usage
See `test/test.ts` for a full example, but it's something like this:

```
const math = require('mathjs')

let A1 = [0,0]
let A2 = [0,1]
let A3 = [1,1]
let A4 = [1,0]

let B1 = [1,1]
let B2 = [1,3]
let B3 = [2,3]
let B4 = [2,1]

let homography = calculateHomography(A1, B1, A2, B2, A3, B3,A4, B4)

let A = [0,0,1]

let B = math.multiply(homography, A)
B[0]=B[0]/B[2]
B[1]=B[1]/B[2]
B[2]=1

// now B should be [1,1,1]

```