import {calculateHomography} from "../index"
import * as Math from 'mathjs'

function test() {
    let correspondances = [
        [[0,0], [1,1]],
        [[1,0], [4,0.2]],
        [[1,1], [2,2]],
        [[0,1], [1,2]],
    ];
    
    let c = correspondances

    let h = calculateHomography(c[0][0], c[0][1], c[1][0], c[1][1],
        c[2][0], c[2][1], c[3][0], c[3][1])
        
    console.log(h)

    correspondances.forEach(corr => {
        let test = (Math.multiply(h, [corr[0][0],corr[0][1],1]) as Math.Matrix).toArray() as number[]

        test[0] = test[0]/test[2]
        test[1] = test[1]/test[2]
        test[2] = 1

        if (test[0] != corr[1][0] && test[1] != corr[1][1]) throw new Error(`It didn't work! - ${corr[0]} -> ${test} instead of ${corr[1]}`)
    
    })

    console.log("it worked!")
}

test()